## Getting Started

1. ```$ git clone git@github.com:GAFuller/rd_template_rails_5.git```
2. ```$ cd rd_template_rails_5```
3. ```$ bundle install```
4. ```rails g rename:app_to New_Name```
5. Update *database.yml* with appropriate database names and usernames
6. ```$ rake db:create```
7. Create *application.yml* in /config directory and add these lines:

  ```YAML
  RAILS_ENV: "development"
  PORT: "3000"
  ```
8. Create *development.log* in /log directory.
8. ```$ git remote rm origin```
9. Create an empty Github repo with your new app name
10. ```$ git remote add origin https://github.com/GAFuller/new-app.git```
11. ```$ git push -u origin master```
12. ```$ rake run```
13. Update the Ruby and Rails version to latest...

## What's Included
### Server
The app is setup for Heroku and so runs on the [Puma](https://github.com/puma/puma) Server
### Database
The apps database is Postgresql so that it works well with Heroku
### HTML
The apps views are in [HAML](https://github.com/indirect/haml-rails).
### Javascript
The app includes a custom light-weight javascript framework as per this [article](https://medium.com/@cblavier/rails-with-no-js-framework-26d2d1646cd). Basically, it loads in a JS object for each page allowing you to have page specific JS and reusable widgets.
### CSS/SASS
The app makesit easy to organise CSS into specific component files within /partials and page specific CSS using SCSS @imports.
### Other Gems
1. [boostrap-sass](https://github.com/twbs/bootstrap-sass)
